public class Doll implements Matryoshka { // стандартная
    private int color;
    private int height;
    private int width;

    public Doll(int color, int height, int width) {
        this.color = color;
        this.height = height;
        this.width = width;
    }

    public int getColor() {
        return color;
    }

    public int getHeight() {
        return height;
    }
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void chooseColor() {
        this.color = 2;
    }

    public void chooseForm() {
        this.height = 3;
        this.width = 2;
    }
}
