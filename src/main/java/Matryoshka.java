public interface Matryoshka {
    public void chooseColor();
    public void chooseForm();
    public int getColor();
    public int getHeight();
    public int getWidth();
}
