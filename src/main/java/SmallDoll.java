public class SmallDoll extends Doll {
    private int color;
    private int height;
    private int width;

    public SmallDoll(int color, int height, int width) {
        super(color, height, width);
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    public void chooseColor() {
        this.color = 1;
    }

    public void chooseForm() {
        this.height = 2;
        this.width = 1;
    }
}
