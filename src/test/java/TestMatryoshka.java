import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMatryoshka {
    @Test
    public void testDoll(){
        Matryoshka matryoshka = new Doll(15,7,8);
        matryoshka.chooseColor();
        matryoshka.chooseForm();
        assertEquals(2, matryoshka.getColor());
        assertEquals(3, matryoshka.getHeight());
        assertEquals(2, matryoshka.getWidth());
    }
    @Test
    public void testSmallDoll(){
        Matryoshka matryoshka = new SmallDoll(15,7,8);
        matryoshka.chooseColor();
        matryoshka.chooseForm();
        assertEquals(1, matryoshka.getColor());
        assertEquals(2, matryoshka.getHeight());
        assertEquals(1, matryoshka.getWidth());
    }
}
